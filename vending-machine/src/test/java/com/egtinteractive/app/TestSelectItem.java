package com.egtinteractive.app;

import static com.egtinteractive.app.VendingMachine.INITIAL_QUANTITY_FOR_ITEM;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.testng.annotations.Test;

public class TestSelectItem {

  @Test(dataProvider = "providerForMessage", dataProviderClass = Providers.class)
  public void noCurrencyState(VendingMachine vendingMachine, ByteArrayOutputStream outContent, PrintStream original){

    vendingMachine.selectItem();

    final String actual = outContent.toString().substring(0, outContent.size() - 1);
    final String expected = VendingMachine.getNoCurrencySelectMessage();
    assertEquals(actual, expected);

    System.setOut(original);
  }

  @Test(dataProvider = "providerForHasCurrencySelectItem", dataProviderClass = Providers.class)
  public void hasCurrencyState(final VendingMachine vendingMachine, final double insertedAmount, final InputStream sysInBackup,
      PrintStream sysOutBackup, ByteArrayOutputStream sysOutNew){

    final Map<String, Items> mapOfAvailableItems = vendingMachine.getAvailableItems();

    vendingMachine.setInsertedAmount(insertedAmount);
    vendingMachine.setState(States.Has_Currency_State);
    vendingMachine.selectItem();

    final int lengthOfEnterSelectionMessage = VendingMachine.getEnterSelectionMessage().length();
    final int lengthOfAnotherDrinkMessage = VendingMachine.getAnotherDrinkMessage().length();
    final int lengthOfYesOrNoMessage = VendingMachine.getYesOrNoMessage().length();
    final int lengthOfInvalidBeverageNameMessage = VendingMachine.getUnvalidBeverageNameMessage().length();

    String availableItemsMessage = mapOfAvailableItems.keySet().toString();

    int startingIndexOfEnterSelectionMessage = 0;
    int startingIndexOfOfAvailableItemsMessage = lengthOfEnterSelectionMessage;
    int startingIndexOfAnotherDrinkMessage = lengthOfEnterSelectionMessage + availableItemsMessage.length();
    int startingIndexOfYesOrNoMessage = lengthOfEnterSelectionMessage + availableItemsMessage.length() + lengthOfYesOrNoMessage;
    int startingIndexOfInvalidBeverageNameMessage;

    final String outPutStream = sysOutNew.toString().substring(0, sysOutNew.size() - 1);
    final String[] orders = outPutStream.replace("Y\n", "").replace("N\n", "").split("\n");
    boolean flag = false;
    for (String order: orders) {

      if (mapOfAvailableItems.get(order).getCount() == 0) {
        mapOfAvailableItems.remove(order);
        availableItemsMessage = mapOfAvailableItems.toString();
        flag = true;
      }

      final Items item = mapOfAvailableItems.get(order);
      item.setCount(item.getCount() - 1);
      mapOfAvailableItems.put(order, item);

      int lengthOfAvailableItemsMessage = availableItemsMessage.length();

      assertEquals(outPutStream.substring(startingIndexOfEnterSelectionMessage, startingIndexOfEnterSelectionMessage + lengthOfEnterSelectionMessage), VendingMachine.getEnterSelectionMessage());
      assertEquals(outPutStream.substring(startingIndexOfOfAvailableItemsMessage, startingIndexOfOfAvailableItemsMessage + lengthOfAvailableItemsMessage), availableItemsMessage);
      if(flag){
        assertEquals(outPutStream.substring(startingIndexOfOfAvailableItemsMessage, startingIndexOfOfAvailableItemsMessage + lengthOfInvalidBeverageNameMessage), VendingMachine.getUnvalidBeverageNameMessage());
        flag = false;
      }
      assertEquals(outPutStream.substring(startingIndexOfAnotherDrinkMessage, startingIndexOfAnotherDrinkMessage + lengthOfAnotherDrinkMessage) , VendingMachine.getAnotherDrinkMessage());
      assertEquals(outPutStream.substring(startingIndexOfYesOrNoMessage, startingIndexOfYesOrNoMessage + lengthOfYesOrNoMessage), VendingMachine.getYesOrNoMessage());

      startingIndexOfEnterSelectionMessage += lengthOfEnterSelectionMessage + lengthOfAvailableItemsMessage + lengthOfAnotherDrinkMessage + lengthOfYesOrNoMessage;
      startingIndexOfOfAvailableItemsMessage += lengthOfAvailableItemsMessage + lengthOfAnotherDrinkMessage + lengthOfYesOrNoMessage + lengthOfEnterSelectionMessage;
      startingIndexOfAnotherDrinkMessage += lengthOfAnotherDrinkMessage + lengthOfYesOrNoMessage + lengthOfEnterSelectionMessage + lengthOfAvailableItemsMessage;
      startingIndexOfYesOrNoMessage += lengthOfYesOrNoMessage + lengthOfEnterSelectionMessage + lengthOfAvailableItemsMessage + lengthOfAnotherDrinkMessage;

    }

    assertEquals(vendingMachine.getProfit(), insertedAmount);
    assertEquals(vendingMachine.getState(), States.Item_Selected_State);

    System.setIn(sysInBackup);
    System.setOut(sysOutBackup);

  }

  @Test(dataProvider = "providerForMessage", dataProviderClass = Providers.class)
  public void itemSelectedState(VendingMachine vendingMachine, ByteArrayOutputStream outContent, PrintStream original){

    vendingMachine.setState(States.Item_Selected_State);
    vendingMachine.selectItem();

    final String actual = outContent.toString().substring(0, outContent.size() - 1);
    final String expected = VendingMachine.getItemSelectedSelectMessage();
    assertEquals(actual, expected);
    assertEquals(vendingMachine.getState(), States.No_Currency_State);

    System.setOut(original);
  }

  @Test(dataProvider = "providerForMessage", dataProviderClass = Providers.class)
  public void itemMadeState(VendingMachine vendingMachine, ByteArrayOutputStream outContent, PrintStream original){

    vendingMachine.setState(States.Item_Made_State);
    vendingMachine.selectItem();

    final String actual = outContent.toString().substring(0, outContent.size() - 1);
    final String expected = VendingMachine.getItemMadeSelectMessage();
    assertEquals(actual, expected);

    System.setOut(original);
  }

  @Test(dataProvider = "providerForMessage", dataProviderClass = Providers.class)
  public void itemTakenState(VendingMachine vendingMachine, ByteArrayOutputStream outContent, PrintStream original){

    vendingMachine.setState(States.Item_Taken_State);
    vendingMachine.selectItem();

    final String actual = outContent.toString().substring(0, outContent.size() - 1);
    final String expected = VendingMachine.getItemTakenSelectMessage();
    assertEquals(actual, expected);
    assertEquals(vendingMachine.getState(), States.No_Currency_State);

    System.setOut(original);
  }

  @Test(dataProvider = "providerForMessage", dataProviderClass = Providers.class)
  public void moneyReturnedState(VendingMachine vendingMachine, ByteArrayOutputStream outContent, PrintStream original){

    vendingMachine.setState(States.Money_Returned_State);
    vendingMachine.selectItem();

    final String actual = outContent.toString().substring(0, outContent.size() - 1);
    final String expected = VendingMachine.getMoneyReturnedMessage();
    assertEquals(actual, expected);

    System.setOut(original);
  }

}
